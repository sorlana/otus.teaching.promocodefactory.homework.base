﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateByIdAsync()
        {
            throw new NotImplementedException();
        }

        public Task<T> UpdateByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<T> DeleteByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<T>> IRepository<T>.GetAllAsync()
        {
            throw new NotImplementedException();
        }

        Task<T> IRepository<T>.GetByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        Task<T> IRepository<T>.CreateByIdAsync()
        {
            throw new NotImplementedException();
        }

        Task<T> IRepository<T>.UpdateByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        Task<T> IRepository<T>.DeleteByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        Task IRepository<T>.GetAsync()
        {
            throw new NotImplementedException();
        }
    }
}